require 'fileutils'
require_relative 'image_color_checker'

puts 'Start'
path = ARGV[0].to_s
unless path.empty?
	abort('This folder does not exist.') unless Dir.exist?(path)
end

ALLOWED_EXTENSIONS = %w(png jpg jpeg)

images = []
ALLOWED_EXTENSIONS.each do |ext|
  files = Dir.glob( [path, "*.#{ext}"].reject(&:empty?).join('/'))
	images = images + files if files.size > 0
end

images_count = images.size
abort('Images not found.') unless images_count > 0
puts "#{images_count} images found."

FOLDERS = %w(color bw)

FOLDERS.each do |folder|
  folder_path = [path, folder].reject(&:empty?).join('/')
	next if File.directory?(folder_path)
	FileUtils.mkdir_p(folder_path)
end

color_count = 0
bw_count = 0
images.each_with_index do |image, idx|
  if ImageColorChecker.is_bw?(image)
		folder = 'bw'
		bw_count += 1
  else
		folder = 'color'
		color_count += 1
  end

	puts "#{idx + 1}/#{images_count}"
	FileUtils.mv( image, [path, folder].reject(&:empty?).join('/') )
end
puts "Finish color: #{color_count}, bw: #{bw_count}"

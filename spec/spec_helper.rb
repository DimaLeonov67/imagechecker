def image_path(filename)
  ['spec', 'support', 'images', filename].join('/')
end


require_relative 'spec_helper'
require_relative '../image_color_checker'

RSpec.describe 'color definition' do
  let(:bw_image) { image_path('bw.jpeg') }
  let(:color_image) { image_path('color.jpg') }

  it 'is color image' do
    expect(ImageColorChecker.is_bw?(color_image)).to be false
  end

  it 'is black white image' do
    expect(ImageColorChecker.is_bw?(bw_image)).to be true
  end
end
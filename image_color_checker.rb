require 'rmagick'

class ImageColorChecker
  PIXEL_COUNT = 5
  NUMBER_COLORS = 100

  attr_reader :image

  class << self

    def is_bw?(filepath)
      new(filepath).check_on_bw
    end

  end

  def initialize(filepath)
    @image = Magick::Image.read(filepath).first
  end

  def check_on_bw
    histogram = image.quantize(NUMBER_COLORS).color_histogram
    pixels = histogram.keys.sort_by {|pixel| histogram[pixel] }

    arr = pixels.first(PIXEL_COUNT).map do |pixel|
      pixel.red == pixel.green && pixel.green == pixel.blue
    end
    arr.delete(false)
    arr.size == PIXEL_COUNT
  end

end
